class NerClient:
    def __init__(self, model, displacy):
        self.model = model
        self.displacy= displacy

    @staticmethod
    def map_label(label): 
        label_map = {
                "PERSON"   : "Person",
                "NORP"     : "Group",
                "LOC"      : "Location",
                "GPE"      : "Location",
                "LANGUAGE" : "Laguage",
            }
        return label_map.get(label)

    def get_ents(self, sentence):
        doc = self.model(sentence)
        html = self.displacy.render(doc, style="ent")
        entitites = [{ "ent": ent.text, "label": map_label(ent.label_) } for ent in doc.ents]
        return {"ents": entitites, "html": ""}



