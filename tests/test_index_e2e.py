import unittest
from selenium import webdriver
from selenium.webdriver.chrome.service import Service
from webdriver_manager.chrome import ChromeDriverManager
from selenium.webdriver.common.by import By

class E2ETests(unittest.TestCase):

    def setUp(self):
        self.driver = webdriver.Chrome(service=Service(ChromeDriverManager().install()))
        self.driver.get("http://127.0.0.1:5000")

    def tearDown(self):
        self.driver.quit()

    def test_browser_title_contains_app_name(self):
        self.assertIn("Named Entity", self.driver.title)
        
    def test_page_heading_is_named_entity_finder(self):
        heading = self._find("heading").text

    def _find(self, val):
        return self.driver.find_element(By.CSS_SELECTOR, value=f"[data-test-id='{val}']")

