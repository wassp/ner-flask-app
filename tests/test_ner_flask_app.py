from ner_flask_app import __version__
import unittest
from ner_flask_app.ner_client import NerClient 
from tests.test_doubles import NerModelTestDouble

def test_version():
    assert __version__ == '0.1.0'


class TestNerClient(unittest.TestCase):

    def test_get_ents_returns_dict_given_empty_causes_empty_spacy_doc_ents(self):
        model = NerModelTestDouble("eng")
        model.returns_doc_ents([])
        ner = NerClient(model)
        ents = ner.get_ents("")
        self.assertIsInstance(ents, dict)

    def test_get_ents_returns_dict_given_nonempty_string_causes_empty_spacy_doc_ents(self):
        model = NerModelTestDouble("eng")
        model.returns_doc_ents([])
        ner = NerClient(model)
        ents = ner.get_ents("Madison is a city in Wisconsin.")
        self.assertIsInstance(ents, dict)

    def test_get_ents_given_spacy_PERSON_is_returned_serialises_to_Person(self):
        model = NerModelTestDouble("eng")
        doc_ents = [{ "text": "Magnus Carlsen", "label_": "PERSON" }] 
        model.returns_doc_ents(doc_ent)
        ner = NerClient(model)
        result = ner.get_ents("...")
        expected_result = { "ents": [{ "ent": "Magnus Carlsen", "label": "Person" }], "html": '' }
        self.assertListEqual(result["ents"], expected_result["ents"])

    def test_get_ents_given_spacy_NORP_is_returned_serialises_to_Group(self):
        model = NerModelTestDouble("eng")
        doc_ents = [{ "text": "Lithuanian", "label_": "NORP" }] 
        model.returns_doc_ents(doc_ents)
        ner = NerClient(model)
        result = ner.get_ents("...")
        expected_result = { "ents": [{ "ent": "Lithuanian", "label": "Group" }], "html": "" }
        self.assertListEqual(result["ents"], expected_result["ents"])

    def test_get_ents_given_spacy_LOC_is_returned_serialises_to_Location(self):
        model = NerModelTestDouble("eng")
        doc_ents = [{ "text": "the ocean", "label_": "LOC" }] 
        model.returns_doc_ents(doc_ents)
        ner = NerClient(model)
        result = ner.get_ents("...")
        expected_result = { "ents": [{ "ent": "the ocean", "label": "Location" }], "html": "" }
        self.assertListEqual(result["ents"], expected_result["ents"])

    def test_get_ents_given_spacy_LANGUAGE_is_returned_serialises_to_Language(self):
        model = NerModelTestDouble("eng")
        doc_ents = [{ "text": "ASL", "label_": "LANGUAGE" }] 
        model.returns_doc_ents(doc_ents)
        ner = NerClient(model)
        result = ner.get_ents("...")
        expected_result = { "ents": [{ "ent": "ASL", "label": "Language" }], "html": "" }
        self.assertListEqual(result["ents"], expected_result["ents"])

    def test_get_ents_given_spacy_GPE_is_returned_serialises_to_Location(self):
        model = NerModelTestDouble("eng")
        doc_ents = [{ "text": "Poland", "label_": "GPE" }] 
        model.returns_doc_ents(doc_ents)
        ner = NerClient(model)
        result = ner.get_ents("...")
        expected_result = { "ents": [{ "ent": "Poland", "label": "Location" }], "html": "" }
        self.assertListEqual(result["ents"], expected_result["ents"])
